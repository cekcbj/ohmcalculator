﻿namespace OhmCalculator.Models
{
    public class ResistorBand
    {
       public ResistorBand()
       {
            
       } 
        
        public ResistorBand(double digit, string color, double multiplier, double tolerance = 0)
        {
            Digit = digit;
            Color = color;
            Multiplier = multiplier;
            Tolerance = tolerance;
        }

        public string Color { get; set; }
        public double ? Digit { get; set; }
        public double ? Multiplier { get; set; }
        public double Tolerance { get; set; }
    } 
}