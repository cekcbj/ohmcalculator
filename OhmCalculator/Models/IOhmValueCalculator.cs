﻿namespace OhmCalculator.Models
{
    public interface IOhmValueCalculator
    {
       int CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor); 
    }
}