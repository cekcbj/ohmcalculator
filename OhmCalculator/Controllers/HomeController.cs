﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using OhmCalculator.Models;

namespace OhmCalculator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            return View(calculator);
        }
        
        public JsonResult CalculateResult(string bandAColor, string bandBColor, string bandCColor, string bandDColor)
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            var value = calculator.CalculateOhmValue(bandAColor, bandBColor, bandCColor, bandDColor);
            var tolerance = calculator.GetTolerance(bandDColor);
            return Json(new {Success = "true", data = new {ohmValue = value, tolerance = tolerance }});
        }
        
    }
}