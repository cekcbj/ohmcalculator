using System;
using System.Web.Mvc;
using NUnit.Framework;
using OhmCalculator.Models;


namespace OhmCalculator
{
    [TestFixture]
    public class OhmCalcTests
    {
        readonly OhmValueCalculator calculator = new OhmValueCalculator();

        [Test]
        public void Calculate_Method_Returns_Int()
        {
            var value = calculator.CalculateOhmValue("yellow", "blue", "black", "white");
            Assert.That(value,  Is.TypeOf<int>());
        }
        [Test]
        public void Calculate_Returns_CorrectValue()
        {
            var value1 = calculator.CalculateOhmValue("yellow", "blue", "black");
            var value2 = calculator.CalculateOhmValue("red", "white", "blue", "gold");
            var value3 = calculator.CalculateOhmValue("green", "violet", "yellow", "red");
            var value4 = calculator.CalculateOhmValue("brown", "gray", "orange", "silver");
            Assert.That(value1, Is.EqualTo(46));
            Assert.That(value2, Is.EqualTo(29 * (int) Math.Pow(10,6)));
            Assert.That(value3, Is.EqualTo(57 * (int) Math.Pow(10,4)));
            Assert.That(value4, Is.EqualTo(18 * (int) Math.Pow(10,3)));

        }

    }



}
